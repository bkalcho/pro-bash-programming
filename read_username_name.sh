#!/bin/bash
#: Title	: read_username_name.sh
#: Date		: 2018-08-22
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Read login name and Name of the user from passwd file.
#: Options	: None

while IFS=: read username a b c name d e 
do
	printf "%-12s %s\n" "$username" "$name"
done < /etc/passwd
