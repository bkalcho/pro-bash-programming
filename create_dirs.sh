#!/bin/bash
#: Title	: create_dirs.sh
#: Date		: 2018-08-18
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Writes a script that creates a directory called bpl inside
#:+		: $HOME. Populate this directory with two subdirectories,
#:+		: bin and scripts.
#: Options	: None

mkdir -p $HOME/bpl/{bin,scripts}
