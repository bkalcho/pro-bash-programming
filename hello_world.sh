#!bin/bash
#: Title	: hello_world.sh
#: Date		: 2018-08-18
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: print Hello, WOrld!
#: Options	: None

print "%s\n" "Hello, World!"
