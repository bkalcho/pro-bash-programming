#!/bin/bash
#: Title	: check_user_input.sh
#: Date		: 2018-08-21
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Write a script that asks the user to enter a number between 20
#:+		: and 30. If the user enters an invalid number or a non-number,
#:+		: ask again. Repeat until a satisfactory number is entered.
#: Options	: None

while true
do
	read -p "Enter an integer number? " number
	case $number in
		*[!0-9]*) printf "%s is non-valid number. Please enter it again\n" "$number" ;;
		*) if [ $number -lt 20 ]
	       	   then
			printf "%d number is lower then 20. Please enter it again\n" "$number"
		   elif [ $number -gt 30 ]
		   then
			  printf "%d number is greater then 30. Please enter it again\n" "$number"
		   else
			  break
		  fi
		 ;;
	 esac
done
