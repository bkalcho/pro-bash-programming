#!/bin/bash
#: Title	: positive_number.sh
#: Date		: 2018-08-20
#: Author	: "Bojan G. Kalicanin" <bojan.itpro@gmail.com>
#: Version	: 1.00
#: Description	: Is this a valid positive integer?
#: Options	: None

case $1 in
	*[!0-9]*) echo false ;;
	*) echo true ;;
esac
